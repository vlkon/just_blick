### Just Blick
An attemp to make a toy LED flashing device with use of ATtiny13 microchip.

The whole design here is not complete since it requres battery connection that is not with the standard connector as seen on PCB.
Simple spring in 3D printed housing was enough for connection.

Code::Blocks was used for compilation of the project (with avrgcc).
KiCad was used for PCB design.
