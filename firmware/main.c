/*
Simple blicker from ATtiny13
3 standard LEDs with different colors
1 LED with blicking propertie
1 button to change operation modes

Port I/O: HIGH==off, LOW==on
Button HIGH==unpressed, LOW==pressed
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/delay.h>

//define hardware connection
#define LED_GREEN       PB3
#define LED_RED         PB0
#define LED_YELLOW      PB4
#define LED_BLICK       PB2
#define LED_MASK        ((1 << LED_GREEN) | (1 << LED_RED) | (1 << LED_YELLOW) | (1 << LED_BLICK))
#define PORT_LED_HIGH   (PORTB | LED_MASK)

#define BUTTON      PB1

//number of timer cycles before the color changes
#define COLOR_FAST              20
#define COLOR_SLOW              100

void button_init(void);
void timer_init(void);
void sleep_idle(void);  //while running timer0
void sleep_deep(void);  //total stop of MCU

volatile uint8_t color_counter = 0x00;  //counts timer cycles how long is color on
volatile uint8_t color_state = 0x00;    //keeps information of which color is currently on

volatile uint8_t mode = 0x00;      //keeps information in which mode of operation is the device
//enumeration for mode variable
#define CASE_MAX                3   //has to be set to the last case value
#define CASE_LED_OFF            0
#define CASE_LED_NATIVE_BLICK   1
#define CASE_LED_COLOR_FAST     2
#define CASE_LED_COLOR_SLOW     3

// various signal flag
volatile uint8_t flag = 0x00;
#define FLAG_BUTTON_SHORT   0x01    //mask for short press
#define BUTTON_SHORT        3       //number of timer cycles before the flag is set
#define FLAG_BUTTON_LONG    0x02    //mask for long press
#define BUTTON_LONG         200     //number of timer cycles before the flag is set


int main(void)
{
    //disable watchdog timer
    MCUSR = 0x00;
    wdt_disable();

    //disable ADC to save power
    ACSR = (1 << ADC);

    //disable PIN as input - those that have LED
    DIDR0 |= (1 << ADC0D) | (1 << ADC2D) | (1 << ADC3D) | (1 << ADC1D) | (1 << AIN0D);

    // turn all LED pins for output with PORTB 1 == off; 0 == on
    PORTB |= LED_MASK;
    DDRB |= LED_MASK;

    button_init();

    timer_init();

    sei();

    while(1) {

        //update signals from button
        if((flag & FLAG_BUTTON_LONG) && (PINB & (1 << BUTTON))) {
            mode = CASE_LED_OFF;   //turn off LEDs
            flag &= ~(FLAG_BUTTON_LONG | FLAG_BUTTON_SHORT);    //reset flags
        } else if((flag & FLAG_BUTTON_SHORT) && (PINB & (1 << BUTTON))) {
            if(++mode > CASE_MAX) {mode = 0x00;}    //a dirty solution - based on CASE_ enumeration it changes its behaviour
            flag &= ~(FLAG_BUTTON_LONG | FLAG_BUTTON_SHORT);    //reset flags
        }

        //update status of LEDs
        switch(mode) {
            case CASE_LED_NATIVE_BLICK:
                PORTB =  ~(1 << LED_BLICK) & PORT_LED_HIGH;
                if(PINB & (1 << BUTTON)) {sleep_deep();}    // the port is actively sinking even in pwr_dwn mode
                break;

            case CASE_LED_COLOR_FAST:
                //Fast changing of pre-defined pattern
                //during odd cases are all LEDs off - see default condition
                switch(color_state) {
                    case 0:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_GREEN);
                        if(++color_counter > COLOR_FAST) {
                            color_counter = 0;
                            ++color_state;
                        }
                        break;

                    case 2:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_RED);
                        if(++color_counter > COLOR_FAST) {
                            color_counter = 0;
                            ++color_state;
                        }
                        break;

                    case 4:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_YELLOW);
                        if(++color_counter > COLOR_FAST) {
                            color_counter = 0;
                            ++color_state;
                        }
                        break;

                    default:
                        PORTB = (PORTB | PORT_LED_HIGH);
                        if(++color_counter > COLOR_FAST) {
                            color_counter = 0;
                            if(++color_state > 5) {color_state=0;}
                        }
                        break;
                }
                sleep_idle();   //need idle for the time
                break;

            case CASE_LED_COLOR_SLOW:
                //Slow changing of pre-defined pattern

                //Without color_counter=0; there is nice fast paced led change until color counter overflows back to 0
                //It is not programaticaly correct to rely on an overflow but it is nice sideeffect for the given application
                switch(color_state) {
                    case 0:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_GREEN);
                        if(++color_counter > COLOR_SLOW) {
                            //color_counter = 0; //without this see comment below case CASE_LED_COLOR_SLOW
                            ++color_state;
                        }
                        break;

                    case 1:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_RED);
                        if(++color_counter > COLOR_SLOW) {
                            //color_counter = 0; //without this see comment below case CASE_LED_COLOR_SLOW
                            ++color_state;
                        }
                        break;

                    case 2:
                        PORTB = (PORTB | PORT_LED_HIGH) & ~(1 << LED_YELLOW);
                        if(++color_counter > COLOR_SLOW) {
                            //color_counter = 0; //without this see comment below case CASE_LED_COLOR_SLOW
                            color_state = 0;
                        }
                        break;
                }
                sleep_idle();   //need idle for the time
                break;

            case CASE_LED_OFF:
            //pass though to default - NO break

            default:
                //turn everything off
                PORTB |= LED_MASK;
                DDRB |= LED_MASK;
                if(!(PINB & (1 << BUTTON))) {
                    sleep_idle();
                } else {
                    sleep_deep();
                }
                break;
        }
    }

    return 0;
}

void button_init(void)
{
    //set pin as input with pull-up enabled
    DDRB &= ~(1 << BUTTON);
    PORTB |= (1 << BUTTON);

    //set interrupt sensing on low level of INT0
    MCUCR &= ~((1 << ISC01) | (1 << ISC00));
}

void timer_init(void)
{
    TCCR0B = 0x00;  //stop all running clock
    TCNT0 = 0x00;   //reset timer counter

    TCCR0A = (1 << WGM01);  //set CTC mode

    OCR0A = 160;    // with F_CPU 16000HZ and prescaler 1 it si 10ms in CTC mode
    //OCR0B = OCR0A;

    TIMSK0 = (1 << OCIE0A);  //enable interrupt on compare A register
}

void sleep_idle(void)
{
    cli();  //disable premature interrupt trigger

    //enable interupt sensing on INT0
    GIMSK |= (1 << INT0);

    //start timer counter - poll every 10 ms after button is pressed
    TCNT0 = 0x00;
    TCCR0B |= (1 << CS00);

    //prepare sleep mode;
    set_sleep_mode(SLEEP_MODE_IDLE);

    //go to sleep
    sleep_enable();
    sei();
    sleep_cpu();
    sleep_disable();
}

void sleep_deep(void)
{
    cli();  //disable premature interrupt trigger

    //disable timer
    TCCR0B &= ~(1 << CS00);
    TCNT0 = 0x00;

    //enable interupt sensing on INT0
    GIMSK |= (1 << INT0);

    //prepare sleep mode;
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);

    //go to sleep
    sleep_enable();
    sei();
    sleep_cpu();
    sleep_disable();
}

ISR(INT0_vect)
{
    uint8_t button_counter = 0x00;

    //count for how long is the button pressed
    do{
        if(++button_counter > BUTTON_LONG) {
            flag |= FLAG_BUTTON_LONG;
        } else if(button_counter > BUTTON_SHORT) {
            flag |= FLAG_BUTTON_SHORT;
        }
        //delay is not perfect since the mcu is working and not sleeping == power consumption
        _delay_ms(10);
    }while(!(PINB & (1 << BUTTON)));

    color_counter = 0x00;
    color_state = 0x00;
}

ISR(TIM0_COMPA_vect)
{
    // So far empty - it is used only to wake up mcu from sleep_idle()
}
